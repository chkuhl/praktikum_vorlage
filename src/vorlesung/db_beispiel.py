import mysql.connector

# Aufbau DB-Verbindung

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    password="secret",
    database="testdb"
)

mycursor = mydb.cursor()


# Einfügen eines Datensatzes
sql = 'INSERT INTO customers (vorname, name) VALUES (%s, %s)'
values = ('Paul', 'Baumann')
mycursor.execute(sql, values)
mydb.commit()

# Lesen (Selektieren) eines Datensatzes
sql = 'SELECT * FROM db.person'
mycursor.execute(sql)
result = mycursor.fetchall()

print(result)

pass
