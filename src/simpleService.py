#!flask/bin/python
from flask import Flask, request, abort

app = Flask(__name__)

@app.route('/anfrage', methods=['GET'])
def create_task():

    print("Auftrag erhalten:")
    return '<h1>AUFTRAG ANGEKOMMEN</h1>', 201

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)