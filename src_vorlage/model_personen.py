import mysql.connector

class DB:
    '''
    Diese Klasse stellt die Anbind an die Datenbank her
    '''
    def __init__(self):
        '''
        Konstruktor der Datenbank. Baut eine Datenbankverbindung auf
        '''
        self.mydb = None
        try:
            self.mydb = mysql.connector.connect(
                pool_name="pool",
                pool_size=5,
                host="34.121.100.12",
                user="grp13",
                password="ttest",
                database="testDB"
            )
        except mysql.connector.errors.InterfaceError:
            print('Konnte keine DB-Verbindung aufbauen.')

    def _reconnect(self):
        '''
        Diese Methode ...
        :return: Liefert xyz zurück
        '''
        if self.mydb == None or not self.mydb.is_connected():
            self.mydb = mysql.connector.connect(
                pool_name="pool",
                pool_size=5,
                host="35.234.79.168",
                user="grp13",
                password="ttest",
                database="testDB"
            )

    def get_person_name(self, customer_id):
        '''
        Liest aus der Datenbank den Namen der Person anhand ihrer ID
        :param customer_id: ID der Person
        :return:
        '''

        # customers = {
        #     "C000001": "Bobby Kopfmann",
        #     "C000002": "Klaus Martinson"
        # }
        self._reconnect()
        pool_connection = mysql.connector.connect(pool_name="pool")
        cursor = pool_connection.cursor()
        sql = "SELECT vorname, name FROM personen WHERE id = %s"
        values = (customer_id,)
        cursor.execute(sql, values)
        result = cursor.fetchall()
        if result:
            pool_connection.close()
            return result[0][0] + " " + result[0][1]
        else:
            pool_connection.close()
            return None

    def insert_buch(self, buch):
        '''
        Fügt ein Buch in die Datenbank ein. Mit dem Einfügen wir das Buch mit der DB-ID ergänzt
        :param buch: Buchobjekt
        :return: Liefert das ergänzte Buchobjekt zurück
        '''
        self._reconnect()
        pool_connection = mysql.connector.connect(pool_name="pool")
        cursor = pool_connection.cursor()
        sql = "INSERT INTO buecher (name) VALUES (%s)"
        values = (buch.name,)
        cursor.execute(sql, values)
        pool_connection.commit()
        buch.id = cursor.lastrowid
        pool_connection.close()
        return buch

    def remove_buch(self, buch):
        '''
        Löscht ein Buch aus der Datenbank (anhand seiner ID)
        :param buch: Buchobjekt
        '''
        self._reconnect()
        pool_connection = mysql.connector.connect(pool_name="pool")
        cursor = pool_connection.cursor()
        sql = "DELETE FROM buecher WHERE id = %s"
        values = (buch.id,)
        cursor.execute(sql, values)
        pool_connection.commit()
        pool_connection.close()

    def get_buecher(self):
        '''
        Liest alle Bücher aus der Datenbank generiert eine Liste von Buchobjekten
        :return: Liefert Liste von Buchobjekten zurück
        '''
        self._reconnect()
        pool_connection = mysql.connector.connect(pool_name="pool")
        cursor = pool_connection.cursor()
        sql = "SELECT id, name FROM buecher"
        cursor.execute(sql)
        result = cursor.fetchall()
        buchliste = []
        for item in result:
            buch = Buch(item[1])
            buch.id = item[0]
            buchliste.append(buch)
        pool_connection.close()
        return buchliste

class PersonListe:
    def __init__(self, db):
        '''
        Konstruktor der Personenliste
        '''
        self.db = db
        self.id = self.__generate_id()
        self.personen = []
        self.status = 'open'

    def add_person(self, person_id):
        '''
        Fügt eine Person anhand ihrer ID in die Liste ein
        :param person_id: Person_ID
        :return: True, falls Name gefunden wurde und Person erfolgreich eingefügt wurde, False sonst
        '''
        person = self.get_person_by_id(person_id)
        if not person:
            person = Person(self.db, person_id)
            if person.name:
                self.personen.append(person)
            return True
        return False

    def remove_person(self, person_id):
        '''
        Löscht eine Person aus der Liste (anhand ihrer ID)
        :param person_id: Person_ID
        :return: True, falls Löschen erfolgreich, False sonst
        '''
        person = self.get_person_by_id(person_id)
        if person:
            self.personen.remove(person)
            return True
        return False

    def get_person_by_id(self, person_id):
        '''
        Ermittelt das Person-Objekt anhand ihrer ID aus der Liste,
        :param person_id: Person_ID
        :return: Person-Objekt, falls existiert, None sonst
        '''
        person = [person for person in self.personen if person.id == person_id]
        if person != []:
            return person[0]
        else:
            return None

    def __generate_id(self):
        '''
        Generieren einer Listen_ID
        :return: Konstant '0001'
        '''
        return '0001'

class Person:
    def __init__(self, db, id):
        '''
        Konstruktor Person, lediglich Zuweisung der Attribute db, id
        :param db: Datenbankverbindung
        :param id: Person_ID
        '''
        self.id = id
        self.name = db.get_person_name(id)

# BUCHLISTE

class Buchliste:
    def __init__(self, db):
        '''
        Konstruktor Buschliste
        :param db: Datenbankverbindung
        '''
        self.db = db
        self.buecher = []

    def synchronize(self):
        '''
        Synchronisiert (importiert) die Buchliste mit der bestehenden Buchliste in der Datenbank
        :return: None
        '''
        self.buecher = db.get_buecher()

    def add_buch(self, buch):
        '''
        Fügt ein Buchobjekt lokal ein und ebenfalls in die DB
        :param buch: Buchobjekt
        :return: None
        '''
        self.db.insert_buch(buch)
        self.buecher.append(buch)

    def delete_buch(self, buch):
        '''
        Löscht ein Buchobjekt lokal und aus der DB
        :param buch: Buchobjekt
        :return: None
        '''
        self.db.remove_buch(buch)
        self.buecher.remove(buch)

class Buch:
    def __init__(self, name):
        '''
        Konstruktor Buchobjekt
        :param name: Name des Buchs
        '''
        self.id = None
        self.name = name

# TEST

#print(db.get_person_name('C000001'))
#bl = Buchliste(db)
#orwell = Buch('Orwell, 1984')
#buchliste.add_buch(orwell)
#bl.synchronize()
#bl.delete_buch(bl.buecher[1])