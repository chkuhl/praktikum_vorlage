import mysql.connector

# Aufbau DB-Verbindung

mydb = mysql.connector.connect(
    host="34.121.100.12",
    user="grp13",
    password="ttest",
    database="testDB"
)

# Lesen
def lesen():
    mycursor = mydb.cursor()
    mycursor.execute("SELECT * FROM personen")
    result = mycursor.fetchall()
    for datensatz in result:
        print(datensatz)

#Schreiben
print("Vor dem Schreiben:")
lesen()

mycursor = mydb.cursor()
sql = "INSERT INTO personen (id, vorname, name) values (%s, %s, %s)"
values = ('C000234','John','Cage')
mycursor.execute(sql, values)

print("Nach dem Schreiben:")
lesen()

#Aktualisieren
sql = "UPDATE personen SET name = %s WHERE vorname = %s"
values = ('Auster', 'Paul')
mycursor.execute(sql, values)

print("Nach dem Aktualisieren:")
lesen()

sql = "DELETE FROM personen WHERE id = %s"
values = ('C000234',)
mycursor.execute(sql, values)
print("Nach dem Löschen:")
lesen()

mydb.commit()
