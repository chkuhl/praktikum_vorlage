#!flask/bin/python
from flask import Flask, request, abort, render_template
from src_vorlage.model_personen import PersonListe, DB
from src_vorlage.view_personen import View

app = Flask(__name__)

view = View()

db = DB()
person_liste = PersonListe(db)

# Dies ist die Route für die Web-Anwendung XYZ
@app.route('/person', methods=['GET'])
def get_tasks():
    try:
        user_id = request.args.get('user_id', default=None, type=str)
        if user_id == "4711":
            return render_template('personen.html', person_liste=person_liste)
        return render_template('keine_personen.html')
    except:
        return render_template('fehler.html')

@app.route('/rest', methods=['POST'])
def event_task():
    try:
        if not request.json or \
                not 'person_id' in request.json or \
                not 'type' in request.json:
            abort(400)
        person_id = request.json['person_id']
        typ = request.json['type']

        result = None
        if typ == 'add':
            result = person_liste.add_person(person_id)
            if result:
                return_value = 'PERSON INSERTED'
            else:
                return_value = 'PERSON NOT INSERTED'
        else:
            result = person_liste.remove_person(person_id)
            if result:
                return_value = 'PERSON DELETED'
            else:
                return_value = 'PERSON NOT DELETED'

        view.print_personen(person_liste)

        return return_value, 201
    except:
        return_value = "FEHLER!"
        return return_value, 400

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)