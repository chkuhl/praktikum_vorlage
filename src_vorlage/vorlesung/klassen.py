class Auto:
    raeder = 4
    zaehler = 0

    @staticmethod
    def km2mile(km):
        return 1.5*km

    def __init__(self, seriennummer, km_stand=0, status='aus'):
        self.seriennummer = seriennummer
        self.km_stand = km_stand
        self.status = status
        type(self).zaehler += 1

    def __del__(self):
        print('Objekt wird zerstört')

    def einschalten(self):
        self.status = 'an'

    def ausschalten(self):
        self.status = 'aus'


#Test
a = Auto(4711)
b = Auto(4712)
pass
