import math
from abc import ABC, abstractmethod

class Gefaess(ABC):
    def __init__(self, status='leer'):
        self.status = status

    def fuellen(self):
        self.status = 'voll'

    def leeren(self):
        self.status = 'leer'

    @abstractmethod
    def volumen(self):
        pass

class Zylinder(Gefaess):
    def __init__(self, radius, hoehe, status='leer'):
        super().__init__(status=status)
        self.radius = radius
        self.hoehe = hoehe

    def volumen(self):
        return self.radius**2 * math.pi * self.hoehe

    def __eq__(self, other):
        if self.volumen() == other.volumen():
            return True
        return False

    def __str__(self):
        return 'Zylinder mit Radius ' + str(self.radius) + ' und Höhe ' + str(self.hoehe)

class Schale(Gefaess):
    def __init__(self, radius, status='leer'):
        super().__init__(status=status)
        self.radius = radius

    def volumen(self):
        return 4/3 * self.radius**3 * math.pi

    def __eq__(self, other):
        if self.volumen() == other.volumen():
            return True
        return False

# Test
liste_von_gefaessen = \
    [ Zylinder(5, 10),
      Schale(7),
      Zylinder(3,20),
      Schale(8),
      Schale(10)
    ]


# Bilden der Summe aller Gefäße
gesamtvolumen = 0.0
for element in liste_von_gefaessen:
    gesamtvolumen += element.volumen()
print(gesamtvolumen)

# Vergleich
z1 = Zylinder(2,2)
z2 = Zylinder(2,3)
if z1 == z2:
    print('Gleiches Volumen')
else:
    print('Unterschiedliches Volumen')

# String-Cast
z = Zylinder(4,8)
print(str(z))