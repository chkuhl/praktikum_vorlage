from unittest import TestCase
from src.vorlesung.vererbung import Zylinder

class Test_Vererbung(TestCase):

    def test_volumen_zylinder(self):
        z = Zylinder(2,2)
        self.assertAlmostEqual(z.volumen(), 25.13, places=2)