class View:
    def print_personen(self, personen_liste):
        for person in personen_liste.personen:
                print("{}: {}".format(
                    person.id,
                    person.name
                ))

    def format_value(self, preis):
            str_value = '{p:3,.2f}'.format(p=preis)
            str_value = str_value.replace('.',',')
            str_value = str_value.replace(',','.', str_value.count(',')-1)
            return str_value
