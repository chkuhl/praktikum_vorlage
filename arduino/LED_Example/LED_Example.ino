/* wiring for the LED
R       = GPIO15 (D8)
G       = GPIO2 (D4)
B       = GPIO0 (D3)
GND     = GND
*/

/* wiring for the Touch-Button
VCC     = 3.3V
I/O     = GPIO16 (D0)
GND     = GND
*/


int redLED = HIGH; //red LED state at end of cycle
int greenLED = LOW; //green LED state at end of cycle
int blueLED = LOW;  //blue LED state at end of cycle
int buttonState = LOW;  //State of the Touch-Button


void setup() {
  //INIT GPIO ---------------------------------------------------------------
  pinMode(16, INPUT);
  pinMode(0, OUTPUT);
  pinMode(2, OUTPUT);
  pinMode(15, OUTPUT);
  //INIT GPIO END -----------------------------------------------------------
  
}

void loop() {
  int redLEDOld = redLED;
  redLED = greenLED;
  greenLED = blueLED;
  blueLED = redLEDOld;
  buttonState = digitalRead(16);  //read button
  if (buttonState == HIGH) {
  	setLED(redLED, greenLED, blueLED); 
    delay(500);
  }
}


void setLED(int redLED, int greenLED, int blueLED) {
	  digitalWrite(0, blueLED);
      digitalWrite(2, greenLED);
      digitalWrite(15, redLED);
}
