#include <ESP8266HTTPClient.h>

String response;  //response from REST-Server

unsigned long connection_check_time = millis();  //used for testing connection interval
unsigned long send_time = millis();  //used for testing connection interval


char server[] = "http://10.3.141.1:5000/todo/api/v1.0/receive"; //Url of REST-Server
IPAddress ip(10,3,141,233); //IP in Network if dhcp fails
HTTPClient http;


void setup() {
  // INIT       BEGIN ---------------------------------------------------
  Serial.begin(9600);    // Initialize serial communications
  delay(250);
  Serial.println(F("Booting...."));
  
  // INIT HTTP-CLIENT BEGIN ---------------------------------------------------
  delay(1000);
  Serial.println("connecting...");

  http.begin(server); //open connection to REST-Server
  int httpCode = http.GET();  //used to "ping" the server

  // if you get a connection, report back via serial:
  if (httpCode > 0) {
    Serial.println("connected");
  }
  else {
    // if you didn't get a connection to the server:
    Serial.println("connection failed");
  }
  http.end();

  // INIT HTTP-CLIENT END ---------------------------------------------------
  
}

void loop() { 
  // check connection again every 10 seconds
  if (millis() - connection_check_time > 10000) {
    http.begin(server); //open connection to REST-Server
    int httpCode = http.GET();  //used to "ping" the server
    http.end();
    connection_check_time = millis();
  }
  // send data every minute
  if (millis() - send_time > 60000) {
	response = send_data(server, "task", "user_id", "article_Id");
	send_time = millis();
  }
}

// Routine to send Data to REST server on the RaspberryPi
String send_data(String server, String type, String customer_id, String article_id){
  http.begin(server); //establish connection
  http.addHeader("Content-Type", "application/json"); //header to signal json-string
  // build json-string
  String request = "{\"customer_id\":\"" + customer_id
              + "\", \"article_id\":\"" + article_id
              + "\", \"type\":\"" + type
              + "\"}";
  int httpCode = http.POST(request);
  String response;
  if (httpCode > 0){
    response = http.getString();
    }
  else {
    response = "No response";
    }
  Serial.println(response);
  http.end(); //close TCP connection and free resources
  return response;
  }

