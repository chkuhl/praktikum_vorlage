#include <SPI.h>
#include "MFRC522.h"

/* wiring the MFRC522 to ESP8266 (ESP-12)
RST     = GPIO5 (D1)
SDA(SS) = GPIO4 (D2)
MOSI    = GPIO13 (D7)
MISO    = GPIO12 (D6)
SCK     = GPIO14 (D5)
GND     = GND 
3.3V    = 3.3V
*/

#define RST_PIN  5  // RST-PIN für RC522 - RFID - SPI - Modul GPIO5 
#define SS_PIN  4  // SDA-PIN für RC522 - RFID - SPI - Modul GPIO4 

const int sector = 1;

String content; //content read from RFID-Module

MFRC522 mfrc522(SS_PIN, RST_PIN); // Create MFRC522 instance
MFRC522::MIFARE_Key key;


void setup() {
  // INIT       BEGIN ---------------------------------------------------
  Serial.begin(9600);    // Initialize serial communications
  delay(250);
  Serial.println(F("Booting...."));
  
  // INIT RC522 BEGIN ---------------------------------------------------
  SPI.begin();           // Init SPI bus
  mfrc522.PCD_Init();    // Init MFRC522
  Serial.println(F("RC522 initialized - Ready for scanning ..."));
  // INIT RC522 END   ---------------------------------------------------

  for (byte i = 0; i < 6; i++) {
        key.keyByte[i] = 0xFF;
    }  
}


void loop() { 
  readRFID(1, key, mfrc522, false, &content); //read content if new Chip in range
  //writeRFID(1, key, mfrc522, false, "I000002");
}



// AUTHENTICATE CARD ---------------------------------
boolean authenticateRFID(int sector, MFRC522::MIFARE_Key key, MFRC522 mfrc522, boolean use_key_A) {
    MFRC522::StatusCode status;
    if (use_key_A) {
      Serial.println(F("Authenticating using key A..."));
      status = (MFRC522::StatusCode) mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, 4*sector+3, &key, &(mfrc522.uid));
    }
    else {
      Serial.println(F("Authenticating using key B..."));
      status = (MFRC522::StatusCode) mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_B, 4*sector+3, &key, &(mfrc522.uid));
    }
    if (status != MFRC522::STATUS_OK) {
        Serial.print(F("PCD_Authenticate() failed: "));
        Serial.println(mfrc522.GetStatusCodeName(status));
        return false;
    }
    return true;
}
// READ THE CARD CONTENT -----------------------------
void readRFID (int sector, MFRC522::MIFARE_Key key, MFRC522 mfrc522, boolean use_key_A, String *result_str)   {
    // Reset the loop if no new card present on the sensor/reader. This saves the entire process when idle.
    if ( ! mfrc522.PICC_IsNewCardPresent()) {
        return; }
    else
        Serial.print(F("Card present "));

    // Select one of the cards
    if ( ! mfrc522.PICC_ReadCardSerial())
        return;
//    else
//        Serial.print(F("Read successful "));

    // Authenticate
    if ( ! authenticateRFID(sector, key, mfrc522, use_key_A))
        return;

    // Show some details of the PICC (that is: the tag/card)
    Serial.print(F("Card UID:"));
    dump_byte_array(mfrc522.uid.uidByte, mfrc522.uid.size);
    Serial.println();
    Serial.print(F("PICC type: "));
    MFRC522::PICC_Type piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);
    Serial.println(mfrc522.PICC_GetTypeName(piccType));

    byte blockAddr = 4 * sector;
    MFRC522::StatusCode status;
    byte buffer[18];
    byte size = sizeof(buffer);

    // Read data from the block
    Serial.print(F("Reading data from block ")); Serial.print(blockAddr);
    Serial.println(F(" ..."));
    status = (MFRC522::StatusCode) mfrc522.MIFARE_Read(blockAddr, buffer, &size);
    if (status != MFRC522::STATUS_OK) {
        Serial.print(F("MIFARE_Read() failed: "));
        Serial.println(mfrc522.GetStatusCodeName(status));
    }
    Serial.print(F("Data in block ")); Serial.print(blockAddr); Serial.println(F(":"));
    dump_byte_array(buffer, 16); Serial.println();
    Serial.println();
    char result[17]; 

    for (byte i = 0; i < 17; i++) {
      result[i] = char(buffer[i]);
    }
    *result_str = String(result);
    Serial.println("#" + *result_str + "#");

    delay(500);
    // Halt PICC
    mfrc522.PICC_HaltA();
    // Stop encryption on PCD
    mfrc522.PCD_StopCrypto1();
    return;
}

// WRITE THE CARD CONTENT -----------------------------
void writeRFID (int sector, MFRC522::MIFARE_Key key, MFRC522 mfrc522, boolean use_key_A, String content)   {
// Reset the loop if no new card present on the sensor/reader. This saves the entire process when idle.
    if ( ! mfrc522.PICC_IsNewCardPresent()) {
        return; }
//    else
//        Serial.print(F("Card present "));

    // Select one of the cards
    if ( ! mfrc522.PICC_ReadCardSerial())
        return;
//    else
//        Serial.print(F("Read successful "));

    // Authenticate
    if ( ! authenticateRFID(sector, key, mfrc522, use_key_A))
        return;

    // Show some details of the PICC (that is: the tag/card)
    Serial.print(F("Card UID:"));
    dump_byte_array(mfrc522.uid.uidByte, mfrc522.uid.size);
    Serial.println();
    Serial.print(F("PICC type: "));
    MFRC522::PICC_Type piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);
    Serial.println(mfrc522.PICC_GetTypeName(piccType));

    byte blockAddr = 4 * sector;
    MFRC522::StatusCode status;
    byte buffer[18];
    byte size = sizeof(buffer);

    byte dataBlock[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

    char content_char[17]; 
    content.toCharArray(content_char, (unsigned int)content.length()+1); 
    byte data[17]; 
    for (byte i = 0; i < 17; i++) {
      if (content.length()-1 < i) 
        dataBlock[i] = byte(0);
      else
        dataBlock[i] = byte(content_char[i]);
    }

    // Write data to the block
    Serial.print(F("Writing data into block ")); Serial.print(blockAddr);
    Serial.println(F(" ..."));
    Serial.println(content + "\n");
    for (byte i = 0; i < 17; i++) 
      Serial.println(content_char[i]);
    dump_byte_array(dataBlock, 16); Serial.println();
    status = (MFRC522::StatusCode) mfrc522.MIFARE_Write(blockAddr, dataBlock, 16);
    if (status != MFRC522::STATUS_OK) {
        Serial.print(F("MIFARE_Write() failed: "));
        Serial.println(mfrc522.GetStatusCodeName(status));
    }
    Serial.println();
    delay(500);
    // Halt PICC
    mfrc522.PICC_HaltA();
    // Stop encryption on PCD
    mfrc522.PCD_StopCrypto1();
    return;
}

// Helper routine to dump a byte array as hex values to Serial
void dump_byte_array(byte *buffer, byte bufferSize) {
  for (byte i = 0; i < bufferSize; i++) {
    Serial.print(buffer[i] < 0x10 ? " 0" : " ");
    Serial.print(buffer[i], HEX);
  }
}
