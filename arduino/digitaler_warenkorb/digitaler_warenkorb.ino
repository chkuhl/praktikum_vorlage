/*
Many thanks to nikxha from the ESP8266 forum
*/

#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <SPI.h>
#include "MFRC522.h"

/* wiring the MFRC522 to ESP8266 (ESP-12)
RST     = GPIO5 (D1)
SDA(SS) = GPIO4 (D2)
MOSI    = GPIO13 (D7)
MISO    = GPIO12 (D6)
SCK     = GPIO14 (D5)
GND     = GND 
3.3V    = 3.3V
*/

/* wiring for the LED
R       = GPIO15 (D8)
G       = GPIO2 (D4)
B       = GPIO0 (D3)
GND     = GND
*/

/* wiring for the Touch-Button
VCC     = 3.3V
I/O     = GPIO16 (D0)
GND     = GND
*/

#define RST_PIN  5  // RST-PIN für RC522 - RFID - SPI - Modul GPIO5 
#define SS_PIN  4  // SDA-PIN für RC522 - RFID - SPI - Modul GPIO4 

const char *ssid =  "filiale-06";     // change according to your Network - cannot be longer than 32 characters!
const char *pass =  "ChangeMe"; // change according to your Network

const int sector = 1;

String content; //content read from RFID-Module
String response;  //response from REST-Server

int buttonState;  //State of the Touch-Button
int buttonStateOld; //State of the Touch-Button from previous cycle

bool remove_article = false;  //remove or add articles

bool LED_off = false; //Used for blinking

int httpCode = 0;

unsigned long blink_time = millis();  //used for blinking
unsigned long delete_time = millis(); //used for deletion after 5 s buttonpress
unsigned long connection_check_time = millis();  //used for testing connection interval

int redLED = LOW; //red LED state at end of cycle
int greenLED = LOW; //green LED state at end of cycle
int blueLED = LOW;  //blue LED state at end of cycle

String Customer_ID = "";  //logged in customer ID

//char server[] = "http://10.3.141.1:5000/todo/api/v1.0/receive"; //Url of REST-Server
char server[] = "http://10.3.141.1:5000/rest"; //Url of REST-Server
IPAddress ip(10,3,141,233); //IP in Network if dhcp fails
HTTPClient http;

MFRC522 mfrc522(SS_PIN, RST_PIN); // Create MFRC522 instance
MFRC522::MIFARE_Key key;

void setup() {
  // INIT       BEGIN ---------------------------------------------------
  Serial.begin(9600);    // Initialize serial communications
  delay(250);
  Serial.println(F("Booting...."));
  
  // INIT WIFI  BEGIN ---------------------------------------------------
  WiFi.begin(ssid, pass);  
  int retries = 0;
  while ((WiFi.status() != WL_CONNECTED) && (retries < 200)) {
    retries++;
    delay(500);
    Serial.print(".");
  }
  if (WiFi.status() == WL_CONNECTED) {
    Serial.println("WiFi connected to " + WiFi.hostname() + ", Client-IP: " + WiFi.localIP().toString());
  }
  // INIT WIFI  END   ---------------------------------------------------
  
  delay(1000);
  // INIT RC522 BEGIN ---------------------------------------------------
  SPI.begin();           // Init SPI bus
  mfrc522.PCD_Init();    // Init MFRC522
  Serial.println(F("RC522 initialized - Ready for scanning ..."));
  // INIT RC522 END   ---------------------------------------------------

  for (byte i = 0; i < 6; i++) {
        key.keyByte[i] = 0xFF;
    }

  //INIT GPIO ---------------------------------------------------------------
  pinMode(16, INPUT);
  pinMode(0, OUTPUT);
  pinMode(2, OUTPUT);
  pinMode(15, OUTPUT);
  //INIT GPIO END -----------------------------------------------------------
  
}

void loop() { 
  // check connection every 5 seconds
  if (httpCode == 0 || millis() - connection_check_time > 5000) {
    delay(1000);
    Serial.println("ping ...");
    http.begin(server); //open connection to REST-Server
    httpCode = http.GET();  //used to "ping" the server
    // if you get a connection, report back via serial:
    if (httpCode > 0) {
      if (LEDSwitchedOff()) {
        Serial.println("connected");
        LED("green");
      }
    }
    else {
      // if you didn't get a connection to the server:
      Serial.println("connection failed");
      LED("none");
    }
    http.end();
    connection_check_time = millis();
  }
  else {
    readRFID(1, key, mfrc522, false, &content); //read content if new Chip in range
    //writeRFID(1, key, mfrc522, false, "I000002");
  
    buttonState = digitalRead(16);  //read button
  
    //in case device is free
    if (Customer_ID == "") {
      remove_article = false;
      if (content.startsWith("C")) {
        response = send_data(server, "log_in", content, "");
        if (response == "log_in") {
          Customer_ID = content;
          Serial.println("logged in");
          LED("yellow");
        }
        else {
          red_pulse();
        }
      }
      else if (content.startsWith("I")){
        red_pulse();
      }
    }
    //if device has user
    else {
      if (buttonState == HIGH && buttonStateOld == LOW) {
        remove_article = ! remove_article;
      } 
      if (buttonState == LOW) {
        delete_time = millis();
      }
      if (millis() - delete_time > 5000) {
        response = send_data(server, "warenkorb_loeschen", Customer_ID, "");
        if (response == "warenkorb_loeschen") {
          Customer_ID = "";
          Serial.println("Warenkorb geloescht");
          LED("green");
        }
        else if (response == "no cart to delete") {
          Customer_ID = "";
          Serial.println("Kein Warenkorb zu loeschen");
          LED("green");
        }
        else {
          red_pulse();
        }
      }
    
    
      if (content.startsWith("C") && content == Customer_ID) {
        response = send_data(server, "log_out", content, "");
        if (response == "log_out") {
          Customer_ID = "";
          Serial.println("logged out");
          LED("green");
        }
        else {
          red_pulse();
        }
      }
      else if (content.startsWith("I")){
        if (remove_article) {
          response = send_data(server, "ware_loeschen", Customer_ID, content);
          if (response == "ware_loeschen") {
            remove_article = ! remove_article;
            Serial.println("Ware geloescht");
            green_pulse();            
          }
          else {
            red_pulse();
          }
        }
        else {
          response = send_data(server, "ware_hinzufuegen", Customer_ID, content);
          if (response == "ware_hinzufuegen") {
            Serial.println("Ware hinzugefügt");
            green_pulse();            
          }
          else {
            red_pulse();
          }
        }
      }
    }  
  
    content = ""; //content from RFID-Chip should only be used once
    buttonStateOld = buttonState; //used to only detect new presses

    //blink state always changes in the background
    if (millis()-blink_time > 200) {
      LED_off = ! LED_off;
      blink_time = millis();
    }

    // LED gets shut off according to blink state only when article can be removed
    if (LED_off && remove_article) {
      digitalWrite(0, LOW);
      digitalWrite(2, LOW);
      digitalWrite(15, LOW);
    }
    else {
      digitalWrite(0, blueLED);
      digitalWrite(2, greenLED);
      digitalWrite(15, redLED);
    }
  }
}


// Routine to controll the LED
void LED(String color) {
  redLED = LOW;
  greenLED = LOW;
  blueLED = LOW;
  if (color == "green") {
    greenLED = HIGH;
  }
  if (color == "blue") {
    blueLED = HIGH;
  }
  if (color == "red") {
    redLED = HIGH;
  }
  if (color == "yellow") {
    greenLED = HIGH;
    redLED = HIGH;
  }
}

boolean LEDSwitchedOff() {
  if (blueLED == LOW && redLED == LOW && greenLED ==LOW) {
    return true;
  }
  return false;
}

// Routine to send Data to REST server on the RaspberryPi
String send_data(String server, String type, String customer_id, String article_id){
  http.begin(server); //establish connection
  http.addHeader("Content-Type", "application/json"); //header to signal json-string
  // build json-string
  String request = "{\"kunde_id\":\"" + customer_id
              + "\", \"ware_id\":\"" + article_id
              + "\", \"type\":\"" + type
              + "\"}";
  int httpCode = http.POST(request);
  String response;
  if (httpCode > 0){
    response = http.getString();
    }
  else {
    response = "No response";
    }
  Serial.println(response);
  http.end(); //close TCP connection and free resources
  return response;
  }

//Routine for red LED pulse
void red_pulse(){
  digitalWrite(0, LOW);
  digitalWrite(2, LOW);
  digitalWrite(15, HIGH);

  delay(1000);

  digitalWrite(0, LOW);
  digitalWrite(2, LOW);
  digitalWrite(15, LOW);
  }

//Routine for red LED pulse
void green_pulse(){
  digitalWrite(0, LOW);
  digitalWrite(2, HIGH);
  digitalWrite(15, LOW);

  delay(1000);

  digitalWrite(0, LOW);
  digitalWrite(2, LOW);
  digitalWrite(15, LOW);
  }


// Helper routine to dump a byte array as hex values to Serial
void dump_byte_array(byte *buffer, byte bufferSize) {
  for (byte i = 0; i < bufferSize; i++) {
    Serial.print(buffer[i] < 0x10 ? " 0" : " ");
    Serial.print(buffer[i], HEX);
  }
}

// AUTHENTICATE CARD ---------------------------------
boolean authenticateRFID(int sector, MFRC522::MIFARE_Key key, MFRC522 mfrc522, boolean use_key_A) {
    MFRC522::StatusCode status;
    if (use_key_A) {
      Serial.println(F("Authenticating using key A..."));
      status = (MFRC522::StatusCode) mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, 4*sector+3, &key, &(mfrc522.uid));
    }
    else {
      Serial.println(F("Authenticating using key B..."));
      status = (MFRC522::StatusCode) mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_B, 4*sector+3, &key, &(mfrc522.uid));
    }
    if (status != MFRC522::STATUS_OK) {
        Serial.print(F("PCD_Authenticate() failed: "));
        Serial.println(mfrc522.GetStatusCodeName(status));
        return false;
    }
    return true;
}
// READ THE CARD CONTENT -----------------------------
void readRFID (int sector, MFRC522::MIFARE_Key key, MFRC522 mfrc522, boolean use_key_A, String *result_str)   {
    // Reset the loop if no new card present on the sensor/reader. This saves the entire process when idle.
    if ( ! mfrc522.PICC_IsNewCardPresent()) {
        return; }
//    else
//        Serial.print(F("Card present "));

    // Select one of the cards
    if ( ! mfrc522.PICC_ReadCardSerial())
        return;
//    else
//        Serial.print(F("Read successful "));

    // Authenticate
    if ( ! authenticateRFID(sector, key, mfrc522, use_key_A))
        return;

    // Show some details of the PICC (that is: the tag/card)
    Serial.print(F("Card UID:"));
    dump_byte_array(mfrc522.uid.uidByte, mfrc522.uid.size);
    Serial.println();
    Serial.print(F("PICC type: "));
    MFRC522::PICC_Type piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);
    Serial.println(mfrc522.PICC_GetTypeName(piccType));

    byte blockAddr = 4 * sector;
    MFRC522::StatusCode status;
    byte buffer[18];
    byte size = sizeof(buffer);

    // Read data from the block
    Serial.print(F("Reading data from block ")); Serial.print(blockAddr);
    Serial.println(F(" ..."));
    status = (MFRC522::StatusCode) mfrc522.MIFARE_Read(blockAddr, buffer, &size);
    if (status != MFRC522::STATUS_OK) {
        Serial.print(F("MIFARE_Read() failed: "));
        Serial.println(mfrc522.GetStatusCodeName(status));
    }
    Serial.print(F("Data in block ")); Serial.print(blockAddr); Serial.println(F(":"));
    dump_byte_array(buffer, 16); Serial.println();
    Serial.println();
    char result[17]; 

    for (byte i = 0; i < 17; i++) {
      result[i] = char(buffer[i]);
    }
    *result_str = String(result);
    Serial.println("#" + *result_str + "#");

    delay(500);
    // Halt PICC
    mfrc522.PICC_HaltA();
    // Stop encryption on PCD
    mfrc522.PCD_StopCrypto1();
    return;
}

// WRITE THE CARD CONTENT -----------------------------
void writeRFID (int sector, MFRC522::MIFARE_Key key, MFRC522 mfrc522, boolean use_key_A, String content)   {
// Reset the loop if no new card present on the sensor/reader. This saves the entire process when idle.
    if ( ! mfrc522.PICC_IsNewCardPresent()) {
        return; }
//    else
//        Serial.print(F("Card present "));

    // Select one of the cards
    if ( ! mfrc522.PICC_ReadCardSerial())
        return;
//    else
//        Serial.print(F("Read successful "));

    // Authenticate
    if ( ! authenticateRFID(sector, key, mfrc522, use_key_A))
        return;

    // Show some details of the PICC (that is: the tag/card)
    Serial.print(F("Card UID:"));
    dump_byte_array(mfrc522.uid.uidByte, mfrc522.uid.size);
    Serial.println();
    Serial.print(F("PICC type: "));
    MFRC522::PICC_Type piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);
    Serial.println(mfrc522.PICC_GetTypeName(piccType));

    byte blockAddr = 4 * sector;
    MFRC522::StatusCode status;
    byte buffer[18];
    byte size = sizeof(buffer);

    byte dataBlock[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

    char content_char[17]; 
    content.toCharArray(content_char, (unsigned int)content.length()+1); 
    byte data[17]; 
    for (byte i = 0; i < 17; i++) {
      if (content.length()-1 < i) 
        dataBlock[i] = byte(0);
      else
        dataBlock[i] = byte(content_char[i]);
    }

    // Write data to the block
    Serial.print(F("Writing data into block ")); Serial.print(blockAddr);
    Serial.println(F(" ..."));
    Serial.println(content + "\n");
    for (byte i = 0; i < 17; i++) 
      Serial.println(content_char[i]);
    dump_byte_array(dataBlock, 16); Serial.println();
    status = (MFRC522::StatusCode) mfrc522.MIFARE_Write(blockAddr, dataBlock, 16);
    if (status != MFRC522::STATUS_OK) {
        Serial.print(F("MIFARE_Write() failed: "));
        Serial.println(mfrc522.GetStatusCodeName(status));
    }
    Serial.println();
    delay(500);
    // Halt PICC
    mfrc522.PICC_HaltA();
    // Stop encryption on PCD
    mfrc522.PCD_StopCrypto1();
    return;
}
