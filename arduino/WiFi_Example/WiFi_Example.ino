#include <ESP8266WiFi.h>

const char *ssid =  "ET-MIS"; // change according to your Network - cannot be longer than 32 characters!
const char *pass =  "MIS-4711"; // change according to your Network

void setup() {
  // INIT       BEGIN ---------------------------------------------------
  Serial.begin(9600);    // Initialize serial communications
  delay(250);
  Serial.println(F("Booting...."));
  
  // INIT WIFI  BEGIN ---------------------------------------------------
  WiFi.begin(ssid, pass);  
  int retries = 0;
  while ((WiFi.status() != WL_CONNECTED) && (retries < 50)) {
    retries++;
    delay(500);
    Serial.print(".");
  }
  if (WiFi.status() == WL_CONNECTED) {
    Serial.println("WiFi connected to " + WiFi.hostname() + ", Client-IP: " + WiFi.localIP().toString());
  }
  // INIT WIFI  END   ---------------------------------------------------
  
}

void loop() { 
}
